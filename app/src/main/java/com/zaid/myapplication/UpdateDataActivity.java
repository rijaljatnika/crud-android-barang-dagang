package com.zaid.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class UpdateDataActivity extends AppCompatActivity {

    private  DataSource datasource;
    private BarangDagang barang;
    int idTerpilih, stok;
    String nama;
    double harga, diskon;
    EditText edNama, edHarga, edStok, edDiskon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_data);

        edNama = findViewById(R.id.update_nama_barang);
        edHarga = findViewById(R.id.update_harga_barang);
        edStok = findViewById(R.id.update_stok_barang);
        edDiskon = findViewById(R.id.update_diskon_barang);

        datasource = new DataSource(this);
        datasource.open();

        idTerpilih = getIntent().getIntExtra("ID_BARANG", 0);
        barang = datasource.readBarangPerId(idTerpilih);

        edNama.setText(barang.getNamaBarang());
        edHarga.setText(String.valueOf(barang.getHargaBarang()));
        edStok.setText(String.valueOf(barang.getStokBarang()));
        edDiskon.setText(String.valueOf(barang.getPersenDiskon()));
    }

    public void simpanData(View view) {
        // ambil dari view
        nama = edNama.getText().toString();
        harga = Double.parseDouble(edHarga.getText().toString());
        stok = Integer.parseInt(edStok.getText().toString());
        diskon = Double.parseDouble(edDiskon.getText().toString());

        // update ke objek barangDadang
        barang.setIdBarang(idTerpilih);
        barang.setNamaBarang(nama);
        barang.setHargaBarang(harga);
        barang.setStokBarang(stok);
        barang.setPersenDiskon(diskon);

        //update data table
        datasource.updateBarangDagang(barang);
        Intent i = new Intent(UpdateDataActivity.this, ListBarangDagang.class);
        startActivity(i);
        datasource.close();
        finish();

    }

    public void batalUpdate(View view) {
        Intent i = new Intent(UpdateDataActivity.this, ListBarangDagang.class);
        startActivity(i);
        datasource.close();
        finish();
    }
}
