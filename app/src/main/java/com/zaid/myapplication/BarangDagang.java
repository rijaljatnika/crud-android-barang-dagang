package com.zaid.myapplication;

public class BarangDagang {
    private int idBarang;
    private String namaBarang;
    private double hargaBarang;
    private int stokBarang;
    private double persenDiskon;

    public BarangDagang() {
    }

    public BarangDagang(String namaBarang, double hargaBarang, int stokBarang) {
        this.namaBarang = namaBarang;
        this.hargaBarang = hargaBarang;
        this.stokBarang = stokBarang;
    }

    public int getIdBarang() {
        return idBarang;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public double getHargaBarang() {
        return hargaBarang;
    }

    public int getStokBarang() {
        return stokBarang;
    }

    public double getPersenDiskon() {
        return persenDiskon;
    }

    public void setIdBarang(int idBarang) {
        this.idBarang = idBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public void setHargaBarang(double hargaBarang) {
        this.hargaBarang = hargaBarang;
    }

    public void setStokBarang(int stokBarang) {
        this.stokBarang = stokBarang;
    }

    public void setPersenDiskon(double persenDiskon) {
        this.persenDiskon = persenDiskon;
    }

    @Override
    public String toString() {
        String str = "[Nama:" + this.namaBarang + " | Harga: " + hargaBarang + "]";
        return str;
    }
}
