package com.zaid.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ListBarangDagang extends Activity {
    private DataSource datasource;
    private ArrayList<BarangDagang> daftarBarang;
    private ListView listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_barang_dagang);

        listview = findViewById(R.id.listBarang);
        daftarBarang = new ArrayList<BarangDagang>();

        //ambil data dari database
        datasource = new DataSource(this);
        datasource.open();
        daftarBarang = datasource.readSemuaBarang();

        //siapkan data ke list
        ArrayAdapter<BarangDagang> adapter =
                new ArrayAdapter<BarangDagang>(
                        this, android.R.layout.simple_list_item_1, daftarBarang);
        listview.setAdapter(adapter);
        listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final int idTerpilih = daftarBarang.get(position).getIdBarang();
                Log.i("info", "ID Terpilih " + idTerpilih);

                final CharSequence[] item = {"Edit", "Delete"};
                AlertDialog.Builder builder = new AlertDialog.Builder(ListBarangDagang.this);
                builder.setTitle("Pilih Aksi");
                builder.setItems(item, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            Intent intent = new Intent(ListBarangDagang.this, UpdateDataActivity.class);
                            intent.putExtra("ID_BARANG", idTerpilih);
                            startActivity(intent);
                            finish();
                        } else {
                            datasource.deleteBarangDagang(idTerpilih);
                            finish();
                            startActivity(getIntent());
                        }
                    }
                });

                builder.create().show();
                return false;
            }
        });
    }


    public void kembaliMain(View view) {
        datasource.close();
        Intent i = new Intent(ListBarangDagang.this, MainActivity.class);
        startActivity(i);
        this.finish();
    }
}
