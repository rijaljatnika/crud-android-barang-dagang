package com.zaid.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText edNama, edHarga, edStock, edDiskon;
    DataSource datasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edNama = findViewById(R.id.createNamaBarang);
        edHarga = findViewById(R.id.createHargaBarang);
        edStock = findViewById(R.id.stokBarang);
        edDiskon = findViewById(R.id.diskonBarang);

        datasource = new DataSource(this);
    }


    public void simpanData(View view) {
        String nama = edNama.getText().toString();
        double harga = Double.parseDouble(edHarga.getText().toString());
        int stok = Integer.parseInt(edStock.getText().toString());
        double diskon = Double.parseDouble(edDiskon.getText().toString());

        datasource.open();
        long id = datasource.createBarang(nama, harga, stok, diskon);
        if (id > 0) {
            //saat data tersimpan, toast muncul
            Toast.makeText(getBaseContext(), "Data tersimpan!",Toast.LENGTH_SHORT).show();
            datasource.close();

            //pindah activity
            Intent i = new Intent(MainActivity.this, ListBarangDagang.class);
            startActivity(i);
            this.finish();
        } else {
            // data gagal tersimpan
            Toast.makeText(getBaseContext(), "Data tidak tesimpan!", Toast.LENGTH_SHORT).show();
        }
    }

    public void lihatDaftar(View view) {
        Intent i = new  Intent(MainActivity.this, ListBarangDagang.class);
        startActivity(i);
        this.finish();
    }

    public void keluar(View view) {
        datasource.close();
        this.finish();
    }

}
