package com.zaid.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class DataSource {

    private SQLiteDatabase database;
    private DBHelper dbHelper;
    private String[] semuaKolom = {
            DBHelper.KOLOM_ID_BARANG,
            DBHelper.KOLOM_NAMA_BARANG,
            DBHelper.KOLOM_HARGA_BARANG,
            DBHelper.KOLOM_STOK_BARANG,
            DBHelper.KOLOM_PERSEN_DISKON
    };

    public DataSource(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void open() {
        try {
            database = dbHelper.getWritableDatabase();
        } catch (SQLException e) {
            Log.w(DataSource.class.getName(), "Error: " + e);
        }
    }

    public void close() {
        dbHelper.close();
    }

    // create barang
    public long createBarang(String nama, double harga, int stok, double diskon) {
        ContentValues data = new ContentValues();
        data.put(DBHelper.KOLOM_NAMA_BARANG, nama);
        data.put(DBHelper.KOLOM_HARGA_BARANG, harga);
        data.put(DBHelper.KOLOM_STOK_BARANG, stok);
        data.put(DBHelper.KOLOM_PERSEN_DISKON, diskon);

        return database.insert(DBHelper.NAMA_TABEL, null, data);
    }


    public ArrayList<BarangDagang> readSemuaBarang() {
        ArrayList<BarangDagang> semuaBarang = new ArrayList<BarangDagang>();
        Cursor cursor = database.query(DBHelper.NAMA_TABEL, semuaKolom, null, null, null, null, DBHelper.KOLOM_NAMA_BARANG + " ASC");

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            BarangDagang barang = cursorToBarangDagang(cursor);
            semuaBarang.add(barang);
            cursor.moveToNext();
        }

        return semuaBarang;
    }

    public BarangDagang cursorToBarangDagang(Cursor cursor) {
        BarangDagang barang = new BarangDagang();

        barang.setIdBarang(Integer.parseInt(cursor.getString(0)));
        barang.setNamaBarang(cursor.getString(1));
        barang.setHargaBarang(Double.parseDouble(cursor.getString(2)));
        barang.setStokBarang(Integer.parseInt(cursor.getString(3)));
        barang.setPersenDiskon(Double.parseDouble(cursor.getString(4)));
        return barang;
    }

    public BarangDagang readBarangPerId(int idTerpilih) {
        BarangDagang barang = new BarangDagang();
        Cursor cursor = database.query(DBHelper.NAMA_TABEL, semuaKolom, "idBarangDagang = " + idTerpilih, null, null, null, null);
        cursor.moveToFirst();
        barang = cursorToBarangDagang(cursor);
        cursor.close();
        return barang;
    }

    public void updateBarangDagang(BarangDagang barangUpdate) {
        ContentValues data = new ContentValues();
        data.put(DBHelper.KOLOM_NAMA_BARANG, barangUpdate.getNamaBarang());
        data.put(DBHelper.KOLOM_HARGA_BARANG, barangUpdate.getHargaBarang());
        data.put(DBHelper.KOLOM_STOK_BARANG, barangUpdate.getStokBarang());
        data.put(DBHelper.KOLOM_PERSEN_DISKON, barangUpdate.getPersenDiskon());

        database.update(DBHelper.NAMA_TABEL, data, "idBarangDagang = " + barangUpdate.getIdBarang(), null );
    }

    public void deleteBarangDagang(int idTerpilih) {
        database.delete(DBHelper.NAMA_TABEL, "IdBarangDagang = " + idTerpilih, null);
    }
}