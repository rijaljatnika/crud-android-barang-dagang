package com.zaid.myapplication;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
    public static final String NAMA_DATABASE = "dbtoko.db";
    public static final int VERSI_DATABASE = 1;

    public static final String NAMA_TABEL = "tblBarangDagang";
    public static final String KOLOM_ID_BARANG = "idBarangDagang";
    public static final String KOLOM_NAMA_BARANG = "namaBarang";
    public static final String KOLOM_HARGA_BARANG = "hargaBarang";
    public static final String KOLOM_STOK_BARANG = "stokBarang";
    public static final String KOLOM_PERSEN_DISKON = "persenBarang";

    public DBHelper(Context context) {
        super(context, NAMA_DATABASE, null, VERSI_DATABASE);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String buat_tabel = "create table " + NAMA_TABEL + "( "
                + KOLOM_ID_BARANG +" integer primary key autoincrement, "
                + KOLOM_NAMA_BARANG +" VARCHAR(100) NOT NULL, "
                + KOLOM_HARGA_BARANG +" double, "
                + KOLOM_STOK_BARANG + " integer, "
                + KOLOM_PERSEN_DISKON +" double );";
        db.execSQL(buat_tabel);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DBHelper.class.getName(), "Upgrading database dari Versi" + oldVersion + "ke " + newVersion + ", akan menimpah data lama");
        db.execSQL("DROP TABLE IF EXISTS " + NAMA_TABEL);
        onCreate(db);
    }
}
